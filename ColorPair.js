import React from 'react';

const MyColors = {1:['black','white'],
2:['green','red'],
3:['blue','orange'],
4:['violet','yellow'],
5:['aqua','bisque'],
6:['black','white'],
7:['green','red'],
8:['blue','orange'],
9:['violet','yellow'],
10:['aqua','bisque']};

function ColorPair () {
    let n = Math.floor(Math.random() * 10)+1;
    return MyColors[n]
}

export default ColorPair;