import React, { useState, useEffect, useCallback } from 'react';
import ColorPair from '../../components/ColorPair';
import MyCircle from '../../components/MyCircle';

const Result = () => {
    const [position, setPosition] = useState({
        top: '0',
        left: '0',
    });
    const [color, setColor] = useState('black');
    const [startLoop, setStartLoop] = useState(0);
    const ref = React.createRef();
    let top = position.top;
    let left = position.left;
    let backgroundColor = color;
    let startColor = color;

    useEffect(() => {
        startLoop > 0 ? circleMove() : console.log("Start");
    }, [startLoop])

    const resetFunction = useCallback(() => {
        setColor(startColor);
    },[])

    function circleMove() {
        let color = ColorPair();
        top = Math.floor(Math.random() * 175);
        backgroundColor = color[0];
        startColor = color[0];
        setPosition({ top: top, left: 475 })
        setColor(backgroundColor);

        setTimeout(function () {
            top = Math.floor(Math.random() * 175);
            backgroundColor = color[1];
            setPosition({ top: top, left: 0 })
            setColor(backgroundColor);
        }, 5000);

        setTimeout(function () {
            setStartLoop(startLoop + 1);
        }, 10000);

    }

    const startMove = e => {
        circleMove();
    }

    return (
        <div>
            <div id="rectangle">
                <MyCircle ref={ref}
                    top={top}
                    left={left}
                    backgroundColor={backgroundColor}>
                </MyCircle>

            </div>
            <button onClick={startMove}>Start</button>
            <button onClick={() => resetFunction()}>Reset color</button>
        </div>
    )
}

export default Result;